<div class="camera_wrap">
    <?php foreach($files as $image) : ?>
        <div data-src="<?php echo Yii::app()->baseUrl.'/uploads/blog_gallery/'.$image->id.'950_450.jpg' ?>"></div>
    <?php endforeach; ?>
</div>

<?php if(count($files) >1): ?>
    <?php
        Yii::app()->clientScript->registerScript(
            'Camera',
            "
                //script
                jQuery('.camera_wrap').camera(
                {
                    pagination: false,
                    thumbnails: false,
                    playPause:false,
                    fx: 'scrollLeft',
                    onLoaded: function() {
                         if(jQuery.browser.msie){
                            var browserVersion = parseInt(jQuery.browser.version,10);
                            if(browserVersion < 9){
                                jQuery('.moments_opener').height(camera_height-rule).delay(200).fadeIn(100);
                                return false;
                            }
                        }
                        var camera_height = jQuery('.camera_wrap').innerHeight();
                         if(parseInt(jQuery(window).innerWidth()) > 767){
                            var rule = (camera_height- jQuery('#great_fancybox').outerHeight())/2;
                            jQuery('#great_fancybox').css('margin-top',rule);
                        }else{
                            jQuery('#great_fancybox').css('margin-top',0).css('padding-top',20);
                        }
                        jQuery('.moments_opener').height(camera_height-rule).delay(200).fadeIn(100);
                    }
                }
                ); //the basic method
            ",
        CClientScript::POS_READY
        );
    ?>
<?php else : ?>
<?php
    Yii::app()->clientScript->registerScript(
        'Camera',
        "
        //script
        jQuery('.camera_wrap').camera(
        {
            pagination: false,
            thumbnails: false,
            navigation: false,
            playPause:false,
            fx: 'scrollLeft',
            onLoaded: function() {
                 if(jQuery.browser.msie){
                    var browserVersion = parseInt(jQuery.browser.version,10);
                    if(browserVersion < 9){
                        jQuery('.moments_opener').height(camera_height-rule).delay(200).fadeIn(100);
                        return false;
                    }
                }
                var camera_height = jQuery('.camera_wrap').innerHeight();
                 if(parseInt(jQuery(window).innerWidth()) > 767){
                    var rule = (camera_height- jQuery('#great_fancybox').outerHeight())/2;
                    jQuery('#great_fancybox').css('margin-top',rule);
                }else{
                    jQuery('#great_fancybox').css('margin-top',0).css('padding-top',20);
                }
                jQuery('.moments_opener').height(camera_height-rule).delay(200).fadeIn(100);
            }
        }
        ); //the basic method
        jQuery('.camera_wrap').cameraStop();

    ",
        CClientScript::POS_READY);
?>
<?php endif; ?>