<?php
/**
 * Class CategoryWidget
 */
class CategoryWidget extends CInputWidget{
    public $category_id = false;
    private $categorys;

    /**
     *
     */
    public function init()
    {
        parent::init();
    }

    /**
     *
     */
    public function run()
    {
        $this->categorys = $this->setCategories();
        $this->render('category',array('model' => $this->categorys,'activeId' => $this->category_id));
    }

    /**
     * @return CActiveRecord[]
     */

    private function getCategories(){
        return BlogCategory::model()->findAll();
    }

    /**
     * @return CActiveRecord[]
     */
    private function setCategories()
    {
        $a = $this->getCategories();
        foreach($a as $category)
        {
            $category->postCount =  count($category->postPublished);
        }

        return $a;
    }

}