<div class="headline">
    <h3>Kategorien</h3>
</div>
<ul class="unstyled">
    <?php foreach ($model as $category) : ?>
        <?php if ($category->postCount > 0) : ?>
            <?php if ($activeId !== false && $category->id == $activeId): ?>
                <li class="active"><?php echo CHtml::link($category->name . "({$category->postCount})", $category->url) ?></li>
            <?php else: ?>
                <li><?php echo CHtml::link($category->name . "({$category->postCount})", $category->url) ?></li>
            <?php endif; ?>
        <?php endif; ?>
    <?php endforeach; ?>
</ul>