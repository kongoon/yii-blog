<?php
/**
 * Brewing
 * TBHtml.php
 * User: brewing
 * Date: 21.03.12
 * Time: 22:49
 * Use http://twitter.github.com/bootstrap/index.html for more Information about Twitter Boostrap
 * This class helps you simple with some features of them and CHtml form Yii
 * I don`t want to set the yii user in panic
 */
class TBHtml extends CHtml
{
    /**
     * @static
     * @param $label
     * @param $url
     * @param array $ajaxOptions
     * @param array $htmlOptions = array('class' => 'btn')
     * @param null $icon
     * @return string Use CHtml::ajaxLink with feature set default $htmlOptions = array('class' => 'btn')
     * and new $icon
     */
    public static function tbAjaxLinkButton($label,$url,$ajaxOptions = array(),$htmlOptions = array('class' => 'btn'),$icon = null)
    {
        if($icon !== null){
            $icon = self::icon($icon);
            $label = $icon.$label;
        }
        $htmlOptions = self::htmlOption($htmlOptions);
        return parent::ajaxLink($label,$url,$ajaxOptions,$htmlOptions);
    }

    /**
     * @static
     * @param $label
     * @param $url
     * @param array $htmlOptions
     * @param null $icon
     * @return string Use CHtml::ajaxLink with feature set default $htmlOptions = array('class' => 'btn')
     * and new $icon
     */
    public static function tbLinkButton($label,$url,$htmlOptions = array('class' => 'btn'),$icon = null)
    {
        if($icon !== null){
            $icon = self::icon($icon);
            $label = $icon.$label;
        }

        $htmlOptions = self::htmlOption($htmlOptions);

        return CHtml::link($label,$url,$htmlOptions);
    }

    public static function alertLinkButton(){

    }
    /**
     * @static
     * @todo implement :)
     */
    public static function tbNavbarMenu()
    {
        return false;
    }

    /**
     * @static
     * @param $class
     * @return string
     */
    private static function icon($class){
        return "<i class='$class'></i>";
    }

    /**
     * @static
     * @param array $htmlOptions
     * @return array|int
     */
    private static function htmlOption($htmlOptions = array()){
        if(isset($htmlOptions['class']) ){
            return $htmlOptions;
        }
        $htmlOptions['class'] = 'btn';
        return $htmlOptions;
    }
}