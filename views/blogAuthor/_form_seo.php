<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'id'=>'blog-seo-form',
    'enableAjaxValidation'=>true,
    'action' => Yii::app()->createAbsoluteUrl('blogadmin/blogAuthor/seo',array('id' => $model->id))
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>
<?php echo $form->textFieldRow($model,'title',array('class'=>'span12','maxlength'=>255)); ?>
<?php echo $form->select2Row($model, 'keywords',
    array(
        'asDropDownList' => false,
        'options' => array(
        'tags' => BlogSeo::model()->getKeywordsAsArray(),
        'placeholder' => 'keywords',
        'width' => '100%',
        'tokenSeparators' => array(',', ' ')
)));?>
<?php echo $form->textAreaRow($model, 'description', array('class'=>'span12', 'rows'=>5)); ?>
<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'type'=>'primary',
        'label'=>$model->isNewRecord ? 'Create' : 'Save',
    )); ?>
</div>

<?php $this->endWidget(); ?>
