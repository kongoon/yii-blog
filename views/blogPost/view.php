<?php
$this->breadcrumbs=array(
	'Blog Posts'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List BlogPost','url'=>array('index')),
	array('label'=>'Create BlogPost','url'=>array('create')),
	array('label'=>'Update BlogPost','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete BlogPost','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BlogPost','url'=>array('admin')),
);
?>

<h1>View BlogPost #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'slug',
		'tags',
		'status',
		'create_time',
		'update_time',
		'author_id',
		'gallery_id',
		'seo_id',
		'css_class',
		'readmore_length',
	),
)); ?>
