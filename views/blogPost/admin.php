<?php
$this->breadcrumbs=array(
	'Blog Posts'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List BlogPost','url'=>array('index')),
	array('label'=>'Create BlogPost','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('blog-post-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Blog Posts</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'blog-post-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'title',
		'slug',
		'tags',
		'status',
		'create_time',
		/*
		'update_time',
		'author_id',
		'gallery_id',
		'seo_id',
		'css_class',
		'readmore_length',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
