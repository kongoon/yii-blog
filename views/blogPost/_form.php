<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'blog-post-form',
	'enableAjaxValidation'=>false,
    'enableClientValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'title',array('class'=>'span12','maxlength'=>128)); ?>

	<?php echo $form->textFieldRow($model,'slug',array('class'=>'span12','maxlength'=>255)); ?>
    <?php
        echo $form->select2Row($model, 'tags',
            array(
                'asDropDownList' => false,
                'options' => array(
                    'tags' => BlogTag::model()->getTagsAsArray(),
                    'placeholder' => 'tags',
                    'width' => '100%',
                    'tokenSeparators' => array(',', ' ')
                )
            )
        );
    ?>
	<?php echo $form->textFieldRow($model,'css_class',array('class'=>'span12','maxlength'=>5)); ?>
	<?php echo $form->textFieldRow($model,'readmore_length',array('class'=>'span12')); ?>
    <?php echo $form->dropDownListRow($model,'status',BlogStatus::items('PostStatus')); ?>
    <?php echo $form->checkBoxListRow($model, 'categoryId', CHtml::listData(BlogCategory::model()->findAll(),'id','name')); ?>
    <?php echo $form->select2Row($model, 'authorName',
        array(
            'asDropDownList' => false,
            'options' => array(
                'tags' => BlogAuthor::model()->getListArray(),
                'placeholder' => 'Author',
                'width' => '100%',
//                'tokenSeparators' => array(',', ' ')
            )));
    ?>
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>
<?php $this->endWidget(); ?>
