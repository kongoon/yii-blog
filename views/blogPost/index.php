<?php
$this->breadcrumbs=array(
	'Blog Posts',
);

$this->menu=array(
	array('label'=>'Create BlogPost','url'=>array('create')),
	array('label'=>'Manage BlogPost','url'=>array('admin')),
);
?>

<h1>Blog Posts</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
