<?php
$this->breadcrumbs=array(
    'BlogAdmin' => $this->homeUrl,
    'Update'-$model->title,
);
?>
<div class="well well-small">
    <h3>Update <i><?php echo CHtml::encode($model->title); ?></i></h3>
</div>
<div class="row-fluid">
    <div class="span8">
        <div class="tabbable tabs-top">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab4" data-toggle="tab"><i class="icon-font"></i> Content</a></li>
                <li><a href="#tab2" data-toggle="tab"><i class="icon-picture"></i> Gallerie</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab4">
                    <?php $this->renderPartial('_contentbox',array('model' => $model)) ?>
                </div>
                <div class="tab-pane" id="tab2">
                    <?php
                    $this->widget('wr_media.components.GalleryManager', array(
                        'gallery' => $model->galleryBehavior->getGallery(),
                        'controllerRoute' => '/wr_media/gallery'
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="tabbable_config tabs-top">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab1" data-toggle="tab"><i class="icon-cog"></i> Post Config</a></li>
                <li><a href="#tab3" data-toggle="tab"><i class="icon-magnet"></i> Seo</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab1">
                    <?php $this->renderPartial('_form', array('model'=>$model)); ?>
                </div>
                <div class="tab-pane" id="tab3">
                    <?php $this->renderPartial('_form_seo',array('model' => $model->seo)) ?>
                </div>
            </div>
        </div>
    </div>
</div>

