<?php
$this->breadcrumbs=array(
	'BlogAdmin'=>array('/blogadmin/blogadmin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage BlogPost','url'=>array('admin')),
);
?>

<h1>Create BlogPost</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>