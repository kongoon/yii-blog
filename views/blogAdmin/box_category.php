<?php
$model=new BlogCategory('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['BlogCategory']))
    $model->attributes=$_GET['BlogCategory'];

$this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'blog-category-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
        'name',
        'slug',
        array(
            'name' =>'order',
            'header' => 'Sortierung',
            'type' => 'raw',
            'value' => 'TBHtml::tbLinkButton(
                "",
                Yii::app()->createAbsoluteUrl("/blogadmin/blogCategory/order", array("id"=>$data->id,"type" => "up")),
                array(
                    "title" => "Hoch",
                    "rel" => "tooltip",
                    "id" => "order_".uniqid()
                ),
                "icon-arrow-up"
            ).TBHtml::tbLinkButton(
                "",
                Yii::app()->createAbsoluteUrl("/blogadmin/blogCategory/order", array("id"=>$data->id,"type" => "down")),
                array(
                    "title" => "Runter",
                    "rel" => "tooltip",
                    "id" => "order_".uniqid()
                ),
                "icon-arrow-down"
            )',
            'filter' => false
        ),
        array
        (
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{update} {delete}',
            'buttons'=>array
            (
                'update' => array
                (
//                    'visible' => '!Yii::app()->user->isGuest&&Yii::app()->user->level >= Yii::app()->params->BK_Admin',
                    'url'=> 'Yii::app()->createUrl("blogadmin/blogCategory/update",array("id" => $data->id))',

                ),
                'delete' => array
                (
//                    'visible' => '!Yii::app()->user->isGuest&&Yii::app()->user->level >= Yii::app()->params->BK_Admin',
                    'url'=> 'Yii::app()->createUrl("blogadmin/blogCategory/delete",array("id" => $data->id))',
                ),
            ),
        ),
    ),
));
?>