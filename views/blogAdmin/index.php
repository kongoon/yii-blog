<?php
$this->breadcrumbs=array(
    'Blog Admin',
);
$this->beginwidget('bootstrap.widgets.TbBox', array(
    'title' => 'Kategorien',
    'headerIcon' => 'icon-home',
    'headerButtons' => array(
        array(
            'class' => 'bootstrap.widgets.TbButtonGroup',
            'buttons'=>array(
                array(
                    'label'=>'',
                    'icon' => 'plus-sign white' ,
                    'type' => 'success',
                    'url'=> Yii::app()->createUrl('blogadmin/blogCategory/create'),
                    'htmlOptions' => array(
                        'rel' => 'tooltip',
                        'title' => 'Neue Kategorie',
                        'id' => 'appendCategory'
                    )
                ),
            ),
        ),
    )));
    $this->renderPartial('box_category');
$this->endWidget();

$this->beginwidget('bootstrap.widgets.TbBox', array(
    'title' => 'Posts',
    'headerIcon' => 'icon-home',
    'headerButtons' => array(
        array(
            'class' => 'bootstrap.widgets.TbButtonGroup',
            'buttons'=>array(
                array(
                    'label'=>'',
                    'icon' => 'plus-sign white' ,
                    'type' => 'success',
                    'url'=> Yii::app()->createUrl('blogadmin/blogPost/create'),
                    'htmlOptions' => array(
                        'rel' => 'tooltip',
                        'title' => 'Neuer Post',
                        'id' => 'appendPost'
                    )
                ),
            ),
        ),
    )));
    $this->renderPartial('box_post');
$this->endWidget();
$this->beginwidget('bootstrap.widgets.TbBox', array(
    'title' => 'Authoren',
    'headerIcon' => 'icon-home',
    'headerButtons' => array(
        array(
            'class' => 'bootstrap.widgets.TbButtonGroup',
        ),
    )));
    $this->renderPartial('box_author');
$this->endWidget();
?>
