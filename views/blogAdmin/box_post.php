<?php
$model=new BlogPost('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['BlogPost']))
    $model->attributes=$_GET['BlogPost'];
$this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'blog-post-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
        'title',
        'slug',
        array
        (
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{update} {delete}',
            'buttons'=>array
            (
                'update' => array
                (
//                    'visible' => '!Yii::app()->user->isGuest&&Yii::app()->user->level >= Yii::app()->params->BK_Admin',
                    'url'=> 'Yii::app()->createUrl("blogadmin/blogPost/update",array("id" => $data->id))',

                ),
                'delete' => array
                (
//                    'visible' => '!Yii::app()->user->isGuest&&Yii::app()->user->level >= Yii::app()->params->BK_Admin',
                    'url'=> 'Yii::app()->createUrl("blogadmin/blogPost/delete",array("id" => $data->id))',
                ),
            ),
        ),
    ),
));
?>