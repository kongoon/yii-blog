<?php
$model=new BlogAuthor('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['BlogPost']))
    $model->attributes=$_GET['BlogPost'];
$this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'blog-author-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
        'name',
        'slug',
        array
        (
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{update}',
            'buttons'=>array
            (
                'update' => array
                (
//                    'visible' => '!Yii::app()->user->isGuest&&Yii::app()->user->level >= Yii::app()->params->BK_Admin',
                    'url'=> 'Yii::app()->createUrl("blogadmin/blogAuthor/update",array("id" => $data->id))',

                ),
            ),
        ),
    ),
));
?>