<?php

/**
 * This is the model class for table "blog_seo".
 *
 * The followings are the available columns in table 'blog_seo':
 * @property integer $id
 * @property string $title
 * @property string $keywords
 * @property string $description
 *
 * The followings are the available model relations:
 * @property BlogCategory[] $blogCategories
 * @property BlogPost[] $blogPosts
 */
class BlogSeo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'blog_seo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('title', 'length', 'max'=>255),
			array('keywords, description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, keywords, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'blogCategories' => array(self::HAS_MANY, 'BlogCategory', 'seo_id'),
			'blogPosts' => array(self::HAS_MANY, 'BlogPost', 'seo_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'keywords' => 'Keywords',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('keywords',$this->keywords,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BlogSeo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    /**
     * @return mixed
     */
    public function getKeywordsAsArray()
    {
        $criteria = new CDbCriteria();
        $criteria->select = 'keywords';
        $keywords = BlogSeo::model()->findAll($criteria);
        $words = array();
        foreach($keywords as $part){
            foreach($this->string2array($part->keywords) as $tag)
            {
                if(!in_array($tag,$words))
                {
                    $words[] = $tag;
                }
            }
        }
        return $words;
    }

    /**
     * @param $tags
     * @return array
     */
    public static function string2array($tags)
    {
        return preg_split('/\s*,\s*/',trim($tags),-1,PREG_SPLIT_NO_EMPTY);
    }
    /**
     * Normalizes the user-entered tags.
     */
    public function normalizeTags($attribute,$params)
    {
        $this->keywords = $this->array2string(array_unique($this->string2array($this->keywords)));
    }

    /**
     * @param $tags
     * @return string
     */
    public static function array2string($tags)
    {
        return implode(', ',$tags);
    }
}
