<?php

/**
 * This is the model class for table "blog_category".
 *
 * The followings are the available columns in table 'blog_category':
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property integer $order
 * @property integer $seo_id
 *
 * The followings are the available model relations:
 * @property BlogSeo $seo
 * @property PostsBlogPost[] $posts
 */
class BlogCategory extends CActiveRecord
{
    public $postCount;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'blog_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, slug, order', 'required'),
			array('order, seo_id', 'numerical', 'integerOnly'=>true),
			array('name, slug', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, slug, order', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'seo' => array(self::BELONGS_TO, 'BlogSeo', 'seo_id'),
			'posts' =>array(self::MANY_MANY, 'BlogPost','blog_post_lookup_category(category_id,post_id)'),
			'postPublished' =>array(self::MANY_MANY, 'BlogPost','blog_post_lookup_category(category_id,post_id)','condition' =>'status >1'),
		);
	}

    /**
     * @return string the URL that shows the detail of the post
     */
    public function getUrl()
    {
        return Yii::app()->createAbsoluteUrl('blog/category', array(
            'slug'=>$this->slug
        ));
    }
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'slug' => 'Category Url',
			'order' => 'Order',
			'seo_id' => 'Seo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('order',$this->order);
		$criteria->compare('seo_id',$this->seo_id);
        $criteria->order = '`order` ASC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BlogCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * @return bool
     */
    protected function beforeValidate()
    {
        if(parent::beforeValidate())
        {
            if($this->isNewRecord)
            {
                //create nice slug
                $this->slug = Sanitizer::getSanitizedUrlValue(strtolower($this->name));
                $count = BlogCategory::model()->count();
                $this->order = $count+1;

            }else{
                //create nice slug
                if(!empty($this->slug))
                    $this->slug = Sanitizer::getSanitizedUrlValue(strtolower($this->slug));
                else
                    $this->slug = Sanitizer::getSanitizedUrlValue(strtolower($this->name));
            }
        }
        return parent::beforeValidate();
    }

    /**
     * create Seo model for Category
     */
    protected function afterValidate()
    {
        if($this->isNewRecord)
        {
            $seo = new BlogSeo();
            $seo->title = $this->name;
            if($seo->save())
                $this->seo_id = $seo->id;

        }
        return parent::afterValidate();
    }

    /**
     * @return bool
     */
    protected function afterDelete()
    {
        if(parent::beforeDelete())
        {
            //delete the seo module
            $this->seo->delete();
            $this->posts->deleteAll();
        }
        return parent::afterDelete();
    }
}
