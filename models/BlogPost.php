<?php

/**
 * This is the model class for table "blog_post".
 *
 * The followings are the available columns in table 'blog_post':
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property string $tags
 * @property integer $status
 * @property string $create_time
 * @property string $update_time
 * @property string $author_id
 * @property integer $gallery_id
 * @property integer $seo_id
 * @property string $css_class
 * @property integer $readmore_length
 *
 * The followings are the available model relations:
 * @property BlogComment[] $blogComments
 * @property BlogSeo $seo
 * @property UsergroupsUser $author
 * @property WrMediaGallery $gallery
 * @property BlogStatus $status0
 * @property BlogPostContent[] $blogPostContents
 * @property BlogPostLookupCategory[] $blogPostLookupCategories
 * @property BlogCategory[] $category
 */
class BlogPost extends CActiveRecord
{
    public $categoryId;
    public $authorName;
    private $_oldtags;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'blog_post';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, slug,categoryId,authorName', 'required'),
			array('status, gallery_id, seo_id, readmore_length', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>128),
			array('slug,authorName', 'length', 'max'=>255),
			array('slug', 'unique', 'message'=>'Diese Url ist bereits vorhanden'),
			array('author_id', 'length', 'max'=>20),
			array('css_class', 'length', 'max'=>5),
			array('tags, create_time, update_time', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, slug, tags, status, create_time, update_time, author_id, gallery_id, seo_id, css_class, readmore_length', 'safe', 'on'=>'search'),
			array('categoryId', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'blogComments' => array(self::HAS_MANY, 'BlogComment', 'post_id'),
			'seo' => array(self::BELONGS_TO, 'BlogSeo', 'seo_id'),
			'author' => array(self::BELONGS_TO, 'BlogAuthor', 'author_id'),
			'gallery' => array(self::BELONGS_TO, 'Gallery', 'gallery_id'),
			'status' => array(self::BELONGS_TO, 'BlogStatus', 'status'),
			'blogPostContents' => array(self::HAS_MANY, 'BlogPostContent', 'post_id','order' => 'blogPostContents.order ASC'),
            'category'=>array(self::MANY_MANY, 'BlogCategory','blog_post_lookup_category(post_id, category_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'slug' => 'Post Url',
			'tags' => 'Tags',
			'status' => 'Status',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'author_id' => 'Author',
			'authorName' => 'Author',
			'gallery_id' => 'Gallery',
			'seo_id' => 'Seo',
			'css_class' => 'Css Class',
			'readmore_length' => 'Readmore Length',
            'categoryId' => 'Kategorie'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        $criteria->with = array('category');
		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('author_id',$this->author_id,true);
		$criteria->compare('gallery_id',$this->gallery_id);
		$criteria->compare('seo_id',$this->seo_id);
		$criteria->compare('css_class',$this->css_class,true);
		$criteria->compare('readmore_length',$this->readmore_length);
		$criteria->compare('category.id',$this->categoryId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    /**
     * @return string the URL that shows the detail of the post
     */
    public function getUrl()
    {
        return Yii::app()->createAbsoluteUrl('blog/post', array(
            'slug'=>$this->slug
        ));
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BlogPost the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * @return bool
     */
    protected function beforeValidate()
    {
        if($this->isNewRecord)
        {
            $this->create_time = $this->update_time = date('Y-m-d H:i:s');

        }else
        {
            $this->update_time = date('Y-m-d H:i:s');
            //create nice slug
        }
        if(!empty($this->authorName))
            $this->author_id = BlogAuthor::model()->checkName($this->authorName);

        if(empty($this->slug))
            $this->slug = Sanitizer::getSanitizedUrlValue(strtolower($this->title));
        else
            $this->slug = Sanitizer::getSanitizedUrlValue(strtolower($this->slug));

        return parent::beforeValidate();
    }
    /**
     * @return array
     */
    public function behaviors()
    {
        return array(
            'galleryBehavior' => array(
                'class' => 'wr_media.components.GalleryBehavior',
                'idAttribute' => 'gallery_id',
                'versions' => array(
                    '100_100' => array(
                        'centeredpreview' => array(100, 100),
                    ),
                    '240_240' => array(
                        'centeredpreview' => array(240, 240),
                    ),
                    '950_450' => array(
                        'centeredpreview' => array(950,450)
                    )
                ),
                'name' => true,
                'description' => true,
            ),
        );
    }
    /**
     * create Seo model for Category
     */
    protected function afterValidate()
    {
        if($this->isNewRecord)
        {
            $seo = new BlogSeo();
            $seo->title = $this->title;
            if($seo->save())
                $this->seo_id = $seo->id;

        }
        return parent::afterValidate();
    }

    /**
     * @return bool
     */
    protected function beforeSave()
    {

        return parent::beforeSave();
    }
    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave()
    {
        parent::afterSave();
        BlogTag::model()->updateFrequency($this->_oldtags, $this->tags);
    }

    /**
     * @return bool
     */
    protected function afterFind()
    {
        $this->authorName = $this->author->name;
        $this->_oldtags=$this->tags;
        $this->getCategoryIds();
        return parent::afterFind();
    }
    /**
     * @return bool
     */
    protected function afterDelete()
    {
        //delete the seo module
        $this->seo->delete();
        $this->blogPostContents->deleteAll();
        BlogTag::model()->updateFrequency($this->tags, '');
        return parent::afterDelete();
    }


    /**
     * @return array a list of links that point to the post list filtered by every tag of this post
     */
    public function getTagLinks()
    {
        $links=array();
        foreach(BlogTag::string2array($this->tags) as $tag)
            $links[]=CHtml::link(CHtml::encode($tag), array('blog/tags', 'tag'=>$tag));
        return $links;
    }

    /**
     * Normalizes the user-entered tags.
     */
    public function normalizeTags($attribute,$params)
    {
        $this->tags=BlogTag::array2string(array_unique(BlogTag::string2array($this->tags)));
    }
    /**
     * @param $value
     */
    public function setCategoryId($value)
    {
        $this->categoryId = $value;
    }

    /**
     * @return null
     */
    public function getCategoryId()
    {
        if ($this->categoryId === null && $this->category !== null)
        {
            $this->categoryId= $this->category;
        }
        return $this->categoryId;
    }


    /**
     * Adds a new comment to this post.
     * This method will set status and post_id of the comment accordingly.
     * @param Comment the comment to be added
     * @return boolean whether the comment is saved successfully
     */
    public function addComment($comment)
    {
        if(Yii::app()->params['commentNeedApproval']){
            $comment->status=BlogComment::STATUS_PENDING;
        }else{
            $comment->status=BlogComment::STATUS_APPROVED;
        }
        $comment->post_id=$this->id;
        return $comment->save();
    }
    /**
     * Note this on return only ids no Objects !!!!
     * @return array
     */
    public function getCategoryIds(){
        $criteria = new CDbCriteria();
        $criteria->compare('post_id',$this->id);
        $catCons = BlogPostLookupCategory::model()->findAll($criteria);
        $ids = array();
        foreach($catCons as $catCon) {
            array_push($ids, $catCon->category_id);
        }
        return $this->categoryId = $ids;
    }
}