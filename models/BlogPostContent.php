<?php

/**
 * This is the model class for table "blog_post_content".
 *
 * The followings are the available columns in table 'blog_post_content':
 * @property integer $id
 * @property string $content
 * @property string $title
 * @property integer $post_id
 * @property integer $order
 *
 * The followings are the available model relations:
 * @property BlogPost $post
 */
class BlogPostContent extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'blog_post_content';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('post_id, order', 'required'),
			array('post_id, order', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			array('content', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, content, title, post_id, order', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'post' => array(self::BELONGS_TO, 'BlogPost', 'post_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'content' => 'Content',
			'title' => 'Title',
			'post_id' => 'Post',
			'order' => 'Order',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('post_id',$this->post_id);
		$criteria->compare('order',$this->order);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BlogPostContent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     *
     */
    protected function afterDelete()
    {
        if($this->newOrder($this->post_id))
            return parent::afterDelete();
    }

    /**
     * @param $post_id
     * @return bool
     */
    public function newOrder($post_id)
    {
        $criteria = new CDbCriteria();
        $criteria->compare('post_id',$post_id);
        $criteria->order = '`order` ASC';
        $model = $this->findAll($criteria);
        $i = 1;

        foreach($model as $block)
        {
            $block->order = $i;
            $block->update();
            $i++;
        }
        return true;
    }
}
