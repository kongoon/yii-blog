<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <link rel="SHORTCUT ICON" href="<?php echo Yii::app()->baseUrl?>/assets/images/favicon.ico" type="image/x-icon"/>
    <link rel="apple-touch-icon-precomposed" href="<?php echo Yii::app()->baseUrl?>/assets/images/apple-touch-icon.png" type="image/x-icon"/>
    <link href="http://fonts.googleapis.com/css?family=Oswald:400,300,700" media="all" rel="stylesheet" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="de"/>
    <?php
        $cs = Yii::app()->clientscript;
//        $cs->scriptMap=array(
//            'jquery.js'=>false,
//        );
        $cs->registerCssFile(Yii::app()->theme->baseUrl.'/assets/css/bootstrap.css');
        $cs->registerCssFile(Yii::app()->theme->baseUrl.'/assets/css/responsive.css');
        $cs->registerCssFile(Yii::app()->theme->baseUrl.'/assets/css/app.css');
        $cs->registerScriptFile('https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?skin=sons-of-obsidian',CClientScript::POS_END);
        $cs->registerCoreScript("jquery",CClientScript::POS_HEAD);
        $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/assets/js/pascalbrewing-min.js',CClientScript::POS_END);
        $cs->registerScript('analytics',"
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

              ga('create', 'UA-34355526-2', 'pascal-brewing.de');
              ga('send', 'pageview');
        ",CClientScript::POS_END);
        ?>
        <!--[if lt IE 8]><script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/pictonic.min.js"></script><![endif]-->
    <title><?php echo CHtml::encode($this->pageTitle) . ' | ' . Yii::app()->name ?></title>
</head>
<body>
<header id="header">
    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <div class="nav-collapse subnav-collapse pull-right collapse" id="top-navigation">
                    <ul class="nav">
                        <li class="active">
                            <a href="<?php echo Yii::app()->createAbsoluteUrl('blog') ?>">Home</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="carousel slide over-something" id="homepage-carousel">
    <div class="carousel-inner slider-w">
        <div class="active item">
            <div class="container">
                <h1 class="slider-header">
                    Developer
                    <strong>
                        Blog
                    </strong>
                </h1>
                <div class="coverflow span12">
                    <article>
                        <div class="ref"><span style="color: #2162AF;background-color: white;padding: 10px;" class="icon-css3-01"></span></div>
                        <div class="ref"><span style="color: #f04530;background-color: white;padding: 10px;" class="icon-html5-02"></span></div>
                        <div class="ref"><span style="color: #F6DC3D;background-color: black;padding: 10px; " class="icon-prog-js01"></span></div>
                        <div class="ref"><span style="color: #4286BC;background-color: white;padding: 10px;" class="icon-prog-yii"></span></div>
                        <div class="ref"><span style="color: #9999cc;background-color: white;padding: 10px;" class="icon-prog-php01"></span></div>
                        <div class="ref"><span style="color: #e97b00;background-color: white;padding: 10px;" class="icon-dbs-mysql"></span></div>
                        <div class="ref"><span  style="color: #000000;background-color: white;padding: 10px;" class="icon-prog-actionscript"></span></div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="headFixed span12 mainCol">
        <div class="row-fluid" id="headCol">
            <div class="span12">
            </div>
        </div>
    </div>
</div>
<div class="container content">
    <div class="row-fluid span12 mainCol">
        <?php echo $content ?>
    </div>
</div>
<footer>
    <div class="pre-footer">
        <div class="container">
            <div class="row">
                <div class="span4">
                    <div class="footer-logo">
                        <a>Pascal<strong>Brewing</strong></a>
                    </div>
                    <ul class="footer-address">
                        <li>
                            <strong>Address:</strong>
                            Rotenwaldstrasse 37<br>
                            70197 Stuttgart
                        </li>
                        <li>
                            <strong>Senior Application Developer</strong>
                        </li>
                    </ul>
                </div>
                <div class="span4">
                    <h5 class="footer-header">Recent Posts</h5>
                    <ul class="footer-list">
                        <li>on work ...</li>
                    </ul>
                </div>
                <div class="span4">
                    <h5 class="footer-header">About me</h5>
                    <ul class="footer-img-list thumbnails">
                        <li class="span1">
                            <a target="_blank" rel="tooltipp" title="becklyn" href="http://www.becklyn.com/profil/team" class="thumbnail">
                               <img src="<?php echo Yii::app()->theme->baseUrl.'/assets/imgs/pascal.png' ?>" />
                            </a>
                        </li>
                        <li class="span1">
                            <a target="_blank" href="https://bitbucket.org/DrMabuse" rel="tooltipp" title="bitbucket" class="thumbnail">
                                <div class="ref">
                                    <span style="color: #205081" class="icon-vc-bitbucket-01"></span>
                                </div>
                            </a>
                        </li>
                        <li class="span1">
                            <a target="_blank" href="https://github.com/brewing" rel="tooltipp" title="github" class="thumbnail">
                                <div class="ref">
                                    <span style="color: #000000" class="icon-github-01"></span>
                                </div>
                            </a>
                        </li>
                        <li class="span1">
                            <a target="_blank" href="https://plus.google.com/u/0/114092769328289111513/posts/p/pub" rel="tooltipp" title="google+" class="thumbnail">
                                <div class="ref">
                                    <span style="color: #DD4B39" class="icon-google__x2B_"></span>
                                </div>
                            </a>
                        </li>
                        <li class="span1">
                            <a target="_blank" href="https://twitter.com/DocMaboos" rel="tooltipp" title="twitter" class="thumbnail">
                                <div class="ref">
                                    <span style="color:#2daddc " class="icon-twitter-3"></span>
                                </div>
                            </a>
                        </li>
                        <li class="span1">
                            <a target="_blank" href="https://www.facebook.com/pascal.brewing" rel="tooltipp" title="facebook" class="thumbnail">
                                <div class="ref">
                                    <span style="color: #3b5998" class="icon-facebook"></span>
                                </div>
                            </a>
                        </li>
                        <li class="span1">
                            <a target="_blank" href="http://www.xing.com/profile/Pascal_Brewing" rel="tooltipp" title="Xing" class="thumbnail">
                                <div class="ref">
                                    <span style="color: #026466" class="icon-xing"></span>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="deep-footer">
        <div class="container">
            <div class="row">
                <div class="span6">
                    <div class="copyright">Copyright © <?php echo date('Y') ?> Pascal Brewing. All rights reserved.</div>
                </div>
                <div class="span6">
                    <ul class="footer-links">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
</html>