<?php $this->beginContent('//layouts/main'); ?>
<div class="row-fluid">
    <div class="span9">
        <?php echo $content ?>
    </div>
    <div class="span3 sideCol">
        <?php $this->widget('blogadmin.extensions.category.CategoryWidget',array('category_id' => false)) ?>
    </div>
</div>
<?php $this->endContent() ?>