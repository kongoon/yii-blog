<?php setlocale(LC_TIME, "de_DE"); ?>
<div class="row-fluid post">
    <div class="span1">
        <div class="post_date">
            <h3><?php echo date('d',strtotime($model->create_time)); ?></h3>
            <h3><?php echo strftime("%h",strtotime($model->create_time)); ?></h3>
        </div>
        <div class="create">
            <h6><?php echo date('d.m.Y',strtotime($model->create_time)); ?></h6>
        </div>
        <div class="categ_bar">
            <h3>Thema</h3>
            <ul class="unstyled">
                <?php $i = 1;$count = count($model->category); foreach($model->category as $category) : ?>
                    <li>
                        <h5><?php echo CHtml::link($i === $count?$category->name:$category->name.',',$category->url )?></h5>
                    </li>
                    <?php $i++; endforeach; ?>
            </ul>
        </div>
    </div>
    <div class="span1"></div>
    <div class="span10">
        <?php if(Yii::app()->user->hasFlash('commentSubmitted')): ?>
            <div class="flash-success">
                <?php echo Yii::app()->user->getFlash('commentSubmitted'); ?>
            </div>
        <?php endif; ?>
        <div class="row">
            <h1 class="slider-header"><?php echo $model->title ?></h1>
        </div>
        <?php if(!empty($model->gallery->galleryPhotos)) :?>
            <div class="row">
                <?php $this->widget('blogadmin.extensions.camera_gallery.Camera',array('files' => $model->gallery->galleryPhotos)) ?>
            </div>
        <?php endif; ?>
        <?php if(!empty($model->blogPostContents)) : ?>
            <?php foreach($model->blogPostContents as $block) : ?>
                <div class="row">
                    <?php if(!empty($block->title)) : ?>
                        <div class="span12">
                            <h4><?php echo $block->title; ?></h4>
                        </div>
                    <?php endif; ?>
                    <?php if(!empty($block->content)) :?>
                        <div class="span12">
                            <?php echo $block->content; ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>

</div>
<div class="row-fluid">
    <div class="span1 offset1">&nbsp;</div>
    <div class="span10">
        <div class="span12">
            <h3>Leave a Comment</h3>
            <div id="cFrim">
            <?php
            $this->renderPartial('_form_comment',array(
                'model'=>$comment,
                'post_id' => $model->id,
                'actionUrl' => $actionUrl
            ));
            ?>
            </div>
            <? Yii::app()->clientScript->registerScript(
                'submitcommentform',
                "
            $('#cFrim form').on('submit',function(event){
                event.preventDefault();
                console.log($('#blog-comment-form').serialize());
                //return;
                $.ajax({
                    url: '{$actionUrl}',
                    data:$(this).serialize(),
                    type:'post'
                }).done(function(data) {
                    $('#cFrim').html(data);
                    console.log(data);
                })
            });
        ",
                CClientScript::POS_READY
            ) ?>
        </div><!-- comments -->
    </div>
</div>
