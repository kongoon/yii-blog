/**
 * Created with JetBrains PhpStorm.
 * User: brewing
 * Date: 27.05.13
 * Time: 00:09
 * To change this template use File | Settings | File Templates.
 */

(function($,bespoke){
    "use strict";
    var CoverFlow = {
        $sectionLength:0,
        $startSlide:3,
        init:function(){
            this.$sectionLength = this._getSectionLength();
            this.$startSlide = Math.ceil(this.$sectionLength*0.5);
            this._initCoverflow();
            this._setStartSlide();
        },
        _initCoverflow:function(){
            bespoke.horizontal.from(
                'article',
                {
                    loop:true
                }
            );
        },
        _setStartSlide:function(){
            var self = this;
            bespoke.slide(3);
        },
        _getSectionLength:function(){
            return $('.coverflow .ref').length;
        }
    };

    $(function(){
        jQuery('body').tooltip({'selector':'[rel=tooltip]'});
        if($('.coverflow').length > 0){
            CoverFlow.init();
        }else{
            if(console !== 'undefined')
                console.info('no coverflow defined');
        }

    });
}(jQuery,bespoke));
