

/* **********************************************
     Begin pictonic.min.js
********************************************** */

/*
 * domready
 * https://github.com/alanclarke/domready.js
 * logic stripped from https://github.com/jquery/
 * Copyright (c) 2012 Alan Clarke
 * Licensed under the GPL license.
 */

/*
 *  after
 *  https://github.com/alanclarke/after.js
 *
 *  Copyright (c) 2012 Alan Clarke
 *  Licensed under the GPL license.
 *
 *  structure and logic largely taken from http://jquery.lukelutman.com/plugins/pseudo/
 *  but uses a different technique to scan and apply rules which avoids the requirement for special syntax
 *  and refactored to remove jquery dependency
 */

function domready(e){var t=this;t.readyWait=1,t.DOMContentLoaded=function(){document.addEventListener?(document.removeEventListener("DOMContentLoaded",t.DOMContentLoaded,!1),t.ready()):document.readyState==="complete"&&(document.detachEvent("onreadystatechange",t.DOMContentLoaded),t.ready())},t.ready=function(n){if(n===!0?--t.readyWait:t.isReady)return;if(!document.body)return setTimeout(t.ready,1);t.isReady=!0;if(n!==!0&&--t.readyWait>0)return;return!e||e()};if(document.readyState==="complete")setTimeout(t.ready,1);else if(document.addEventListener)document.addEventListener("DOMContentLoaded",t.DOMContentLoaded,!1),window.addEventListener("load",t.ready,!1);else{document.attachEvent("onreadystatechange",t.DOMContentLoaded),window.attachEvent("onload",t.ready);var n=!1;try{n=!window.frameElement&&document.documentElement}catch(r){}n&&n.doScroll&&function i(){if(!t.isReady){try{n.doScroll("left")}catch(e){return setTimeout(i,50)}t.ready()}}()}}function getElements(e,t){if(!e||!e.length||e.length===0)return[];var n=[t||document.body],r=[];e=e.split(" ");for(var i=0;i<e.length;i++){var s={id:"",tag:"*",clas:[]},o,u,a=e[i];while(a.length>0)u=a.search(/.[#\.]/)+1||a.length,o=a.substr(0,u),o.substr(0,1)==="#"?s.id=o.substr(1):o.substr(0,1)==="."?s.clas.push(o.substr(1)):s.tag=o,a=a.substr(u);while(n.length>0){var f=[],l=n.shift().getElementsByTagName(s.tag);for(var c=0;c<l.length;c++)f.push(l[c]);while(f.length>0){var h=!0,p=f.shift();s.id&&p.id!==s.id&&(h=!1);for(c=0;c<s.clas.length;c++){var d=s.clas[c];if(d.charAt(d.length-1)==="*"){var v="\\s"+d.substr(0,d.length-1)+"\\S*\\s";if(!(new RegExp(v)).test(" "+p.className+" ")){h=!1;break}}else if((" "+p.className+" ").indexOf(" "+d+" ")===-1){h=!1;break}}h&&r.push(p)}}n=r,r=[]}return n}typeof define=="function"&&define.amd&&define("src/domready/domready",[],domready),function(){typeof define=="function"&&define.amd&&define("src/getElements",[],getElements)}();var afterjs;(function(){if(typeof e=="undefined")var e;(function(e){function t(t){return function(n){function i(e){if(e&&e.length){var t=e.match(r.text)[1],n=t.match(r.url);return n?'<img src="'+n[1]+'" />':t}}function s(e,n,r){var s=r.style;for(var o=0;o<n.length;o++){var u=n[o],a=t(".pseudo-after",n[o]);a.length||(a=document.createElement("span"),u.appendChild(a)),a.className="pseudo-after",a.innerHTML=i(r.style.content);for(var f in s)if(s[f])try{a.style[f]=s[f]}catch(l){}}}t=n.no_jquery?t:e||t;var r={text:/^['"]?(.+?)["']?$/,url:/^url\(["']?(.+?)['"]?\)$/},o=t(".icon-*");for(var u=0;u<o.length;u++)/(^|\s)pictonic(\s|$)/.test(o[u].className)||(o[u].className+=" pictonic");for(var u=0;u<document.styleSheets.length;u++)try{var a;document.styleSheets[u].cssRules?a=document.styleSheets[u].cssRules:document.styleSheets[u].rules?a=document.styleSheets[u].rules:a=[];for(var f=0;f<a.length;f++)try{var l=a[f],c=l.selectorText.replace(/:+\w+/gi,"");if(n.force||/:+unknown/gi.test(l.selectorText))if(l.style.content){var o=t(c.toString());o.length&&s("before",o,l)}}catch(h){}}catch(h){}return!n.callback||n.callback()}}typeof define=="function"&&define.amd?define("src/after",["src/getElements"],t):getElements&&(afterjs=t(getElements))})(e)})(),function(){e=window.afterjs_opts;if(typeof e!="undefined"){if(e.manual_run)return}else var e={};typeof define=="function"&&define.amd?define("src/after.run.js",["src/domready/domready","src/after"],function(t,n){return t(function(){n(e)})}):domready&&afterjs&&domready(function(){afterjs(e)})}();

/* **********************************************
     Begin bespoke.js
********************************************** */

/*!
 * Bespoke.js v0.0.1-alpha-16
 *
 * Copyright 2013, Mark Dalgleish
 * This content is released under the MIT license
 * http://mit-license.org/markdalgleish
 */

(function(moduleName, window, document) {
    var from = function(selector, selectedPlugins) {
            var parent = document.querySelector(selector),
                slides = [].slice.call(parent.children, 0),
                activeSlide = slides[0],
                deckListeners = {},

                activate = function(index) {
                    if (!slides[index]) {
                        return;
                    }

                    fire(deckListeners, 'deactivate', {
                        slide: activeSlide,
                        index: slides.indexOf(activeSlide)
                    });

                    activeSlide = slides[index];

                    slides.map(deactivate);

                    fire(deckListeners, 'activate', {
                        slide: activeSlide,
                        index: index
                    });

                    addClass(activeSlide, 'active');
                    removeClass(activeSlide, 'inactive');
                },

                deactivate = function(slide, index) {
                    var offset = index - slides.indexOf(activeSlide),
                        offsetClass = offset > 0 ? 'after' : 'before';

                    ['before(-\\d+)?', 'after(-\\d+)?', 'active', 'inactive'].map(removeClass.bind(null, slide));

                    slide !== activeSlide &&
                    ['inactive', offsetClass, offsetClass + '-' + Math.abs(offset)].map(addClass.bind(null, slide));
                },

                next = function() {
                    var nextSlideIndex = slides.indexOf(activeSlide) + 1;

                    fire(deckListeners, 'next', {
                        slide: activeSlide,
                        index: slides.indexOf(activeSlide)
                    }) && activate(nextSlideIndex);
                },

                prev = function() {
                    var prevSlideIndex = slides.indexOf(activeSlide) - 1;

                    fire(deckListeners, 'prev', {
                        slide: activeSlide,
                        index: slides.indexOf(activeSlide)
                    }) && activate(prevSlideIndex);
                },

                deck = {
                    on: on.bind(null, deckListeners),
                    off: off.bind(null, deckListeners),
                    slide: activate,
                    next: next,
                    prev: prev,
                    parent: parent,
                    slides: slides
                };

            activate(0);

            addClass(parent, 'parent');

            slides.map(function(slide) {
                addClass(slide, 'slide');
            });

            Object.keys(selectedPlugins || {}).map(function(pluginName) {
                var config = selectedPlugins[pluginName];
                config && plugins[pluginName](deck, config === true ? {} : config);
            });

            decks.push(deck);

            return deck;
        },

        decks = [],

        bespokeListeners = {},

        on = function(listeners, eventName, callback) {
            (listeners[eventName] || (listeners[eventName] = [])).push(callback);
        },

        off = function(listeners, eventName, callback) {
            listeners[eventName] = (listeners[eventName] || []).filter(function(listener) {
                return listener !== callback;
            });
        },

        fire = function(listeners, eventName, payload) {
            return (listeners[eventName] || [])
                .concat((listeners !== bespokeListeners && bespokeListeners[eventName]) || [])
                .reduce(function(notCancelled, callback) {
                    return notCancelled && callback(payload) !== false;
                }, true);
        },

        addClass = function(el, cls) {
            el.classList.add(moduleName + '-' + cls);
        },

        removeClass = function(el, cls) {
            el.className = el.className
                .replace(new RegExp(moduleName + '-' + cls +'(\\s|$)', 'g'), ' ')
                .replace(/^\s+|\s+$/g, '');
        },

        callOnAllInstances = function(method) {
            return function(arg) {
                decks.map(function(deck) {
                    deck[method].call(null, arg);
                });
            };
        },

        bindPlugin = function(pluginName) {
            return {
                from: function(selector, selectedPlugins) {
                    (selectedPlugins = selectedPlugins || {})[pluginName] = true;
                    return from(selector, selectedPlugins);
                }
            };
        },

        makePluginForAxis = function(axis) {
            return function(deck) {
                var startPosition,
                    delta;

                document.addEventListener('keydown', function(e) {
                    var key = e.which;

                    if (axis === 'X') {
                        key === 37 && deck.prev();
                        (key === 32 || key === 39) && deck.next();
                    } else {
                        key === 38 && deck.prev();
                        (key === 32 || key === 40) && deck.next();
                    }
                });

                deck.parent.addEventListener('touchstart', function(e) {
                    if (e.touches.length) {
                        startPosition = e.touches[0]['page' + axis];
                        delta = 0;
                    }
                });

                deck.parent.addEventListener('touchmove', function(e) {
                    if (e.touches.length) {
                        e.preventDefault();
                        delta = e.touches[0]['page' + axis] - startPosition;
                    }
                });

                deck.parent.addEventListener('touchend', function() {
                    Math.abs(delta) > 50 && (delta > 0 ? deck.prev() : deck.next());
                });
            };
        },

        plugins = {
            horizontal: makePluginForAxis('X'),
            vertical: makePluginForAxis('Y')
        };

    window[moduleName] = {
        from: from,
        slide: callOnAllInstances('slide'),
        next: callOnAllInstances('next'),
        prev: callOnAllInstances('prev'),
        horizontal: bindPlugin('horizontal'),
        vertical: bindPlugin('vertical'),
        on: on.bind(null, bespokeListeners),
        off: off.bind(null, bespokeListeners),
        plugins: plugins,
        decks: decks
    };

}('bespoke', this, document));

/* **********************************************
     Begin bespoke-loop.js
********************************************** */

(function(bespoke) {

    bespoke.plugins.loop = function(deck) {
        deck.on('prev', function(e) {
            if (e.index === 0) {
                deck.slide(deck.slides.length - 1);
            }
        });

        deck.on('next', function(e) {
            if (e.index === deck.slides.length - 1) {
                deck.slide(0);
            }
        });
    };

}(bespoke));

/* **********************************************
     Begin coverflow.js
********************************************** */

/**
 * Created with JetBrains PhpStorm.
 * User: brewing
 * Date: 27.05.13
 * Time: 00:09
 * To change this template use File | Settings | File Templates.
 */

(function($,bespoke){
    "use strict";
    var CoverFlow = {
        $sectionLength:0,
        $startSlide:3,
        init:function(){
            this.$sectionLength = this._getSectionLength();
            this.$startSlide = Math.ceil(this.$sectionLength*0.5);
            this._initCoverflow();
            this._setStartSlide();
        },
        _initCoverflow:function(){
            bespoke.horizontal.from(
                'article',
                {
                    loop:true
                }
            );
        },
        _setStartSlide:function(){
            var self = this;
            bespoke.slide(3);
        },
        _getSectionLength:function(){
            return $('.coverflow .ref').length;
        }
    };

    $(function(){
        jQuery('body').tooltip({'selector':'[rel=tooltip]'});
        if($('.coverflow').length > 0){
            CoverFlow.init();
        }else{
            if(console !== 'undefined')
                console.info('no coverflow defined');
        }

    });
}(jQuery,bespoke));
