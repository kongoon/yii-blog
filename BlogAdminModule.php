<?php

class BlogAdminModule extends CWebModule
{
    public $assetsUrl;
    /**
     * @var boolean whether to force copying of assets.
     * Useful during development and when upgrading the module.
     */
    public $forceCopyAssets = true;

    public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'blogadmin.models.*',
			'blogadmin.components.*',
			'blogadmin.helpers.*',

		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
            $this->assetsUrl = $this->getAssetsUrl();
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}

    /**
     * Returns the URL to the published assets folder.
     * @return string the URL.
     */
    protected function getAssetsUrl()
    {
        if (isset($this->assetsUrl))
            return $this->assetsUrl;
        else
        {
            $assetsPath = Yii::getPathOfAlias('blogadmin.assets');
            $assetsUrl = Yii::app()->assetManager->publish($assetsPath, false, -1, $this->forceCopyAssets);

            return $this->assetsUrl = $assetsUrl;
        }
    }
}
