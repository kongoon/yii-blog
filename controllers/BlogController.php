<?php
/**
 * Class BlogController
 */
class BlogController extends EController {

    public $layout;
    /**
     * same like __construct
     * imported blog Module and media Module
     */
    public function init()
    {
        Yii::app()->theme = 'blog';
        $this->layout = '//layouts/column_2';
        // import the module-level models and components
        Yii::import('blogadmin.models.*');
        Yii::import('blogadmin.components.*');
        Yii::import('wr_media.models.*');
        Yii::import('wr_media.components.*');

    }

    /**
     * limit 10 create Desc
     * start Page Blog
     */
    public function actionIndex()
    {
        $this->setIndexSeo();
        $criteria = new CDbCriteria();
        $criteria->compare('status',2);
        $criteria->order = 'create_time DESC';
        $criteria->limit = '10';
        $model = BlogPost::model()->findAll($criteria);
        $this->render('start',array('model' => $model));
    }

    /**
     * Show the category
     * @param $slug
     */
    public function actionCategory($slug)
    {
        $criteria = new CDbCriteria();
        $criteria->compare('slug',$slug);
        $model = BlogCategory::model()->find($criteria);
        $model = BlogCategory::model()->with('posts','seo')->findByPk($model->id);

        // Set Seo Config
        $seoModel = new SeoModel();
        $this->pageTitle = $model->seo->title;
        $seoModel->keywords = $model->seo->keywords;
        $seoModel->description = $model->seo->description;
        $seoModel->setMetatags();
        //end Seo

        $this->render('category',array('model' => $model));
    }

    /**
     * Show the Single Post
     * @param $slug
     */
    public function actionPost($slug){

        $criteria = new CDbCriteria();
        $criteria->compare('slug',$slug);
        $model = BlogPost::model()->find($criteria);
        $model = BlogPost::model()->findByPk($model->id);
        $comment = new BlogComment();
        $comment->post_id = $model->id;
        // Set Seo Config
        $seoModel = new SeoModel();
        $this->pageTitle = $model->seo->title;
        $seoModel->keywords = $model->seo->keywords;
        $seoModel->author = $model->author->url;
        $seoModel->description = $model->seo->description;
        $seoModel->published_time = $model->create_time;
        $seoModel->modified_time = $model->update_time;
        $seoModel->setMetatags();
        //end Seo



        $this->render('post',array('model' => $model,'comment' => $comment,'actionUrl' => Yii::app()->createUrl('blog/newComment',array('post_id' => $model->id))));
    }

    public function actionNewComment($post_id){
        $model = new BlogComment();
        $model->post_id = $post_id;

        if(isset($_POST['BlogComment']))
        {
            $model->attributes = $_POST['BlogComment'];

            if($model->validate()){
                $model->save();
                echo '<div class="alert alert-success">Thanks for your comment it will be approved next time</div>';
                Yii::app()->end();
            }
            echo $this->renderPartial('_form_comment',array('model' => $model,'post_id' => $post_id,'actionUrl' => Yii::app()->createUrl('blog/newComment',array('post_id' => $model->id))));
        }
    }
    /**
     * Show the posts by Author
     * @param $slug
     */
    public function actionAuthor($slug)
    {

        $criteria = new CDbCriteria();
        $criteria->compare('slug',$slug);
        $model = BlogAuthor::model()->find($criteria);
        $model = BlogAuthor::model()->findByPk($model->id);

        // Set Seo Config
        $seoModel = new SeoModel();
        $this->pageTitle = $model->seo->title;
        $seoModel->keywords = $model->seo->keywords;
        $seoModel->author = $model->url;
        $seoModel->description = $model->seo->description;
        $seoModel->setMetatags();
        //end Seo

        $this->render('author',array('model' => $model));
    }

    private function setIndexSeo()
    {
        $seoModel = new SeoModel();
        $this->pageTitle = 'Home';
        $seoModel->keywords = 'blog,developer';
        $seoModel->description = 'All around Yii Framework. Tutorials , Shortcodes etc';
        $seoModel->setMetatags();
    }

    /**
     * Creates a new comment.
     * This method attempts to create a new comment based on the user input.
     * If the comment is successfully created, the browser will be redirected
     * to show the created comment.
     * @param Post the post that the new comment belongs to
     * @return Comment the comment instance
     */
    protected function newComment($post)
    {
        $comment=new BlogComment();
        if(isset($_POST['ajax']) && $_POST['ajax']==='blog-comment-form')
        {
            echo CActiveForm::validate($comment);
            Yii::app()->end();
        }

        if(isset($_POST['BlogComment']))
        {
            $comment->attributes=$_POST['BlogComment'];
            if($post->addComment($comment))
            {
                if($comment->status==BlogComment::STATUS_PENDING)
                    Yii::app()->user->setFlash('success','Danke für Ihr Kommentar');
                $this->refresh();
            }
        }
        return $comment;
    }

}