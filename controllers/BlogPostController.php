<?php

class BlogPostController extends EController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
    public $homeUrl;

    /**
     * like __construct
     */
    public function init(){
        $this->homeUrl = Yii::app()->createUrl('blogadmin/blogAdmin');
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view','order','create','update','admin','delete','seo','addContentToPost'),
                'users'=>array('*'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadPostModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new BlogPost;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BlogPost']))
		{
			$model->attributes=$_POST['BlogPost'];
            $model->authorName = $_POST['BlogPost']['authorName'];
            $model->categoryId  = $_POST['BlogPost']['categoryId'];
            if($model->save()){
                foreach($model->categoryId as $category){
                    $lookup = new BlogPostLookupCategory();
                    $lookup->category_id= $category;
                    $lookup->post_id = $model->id;
                    $lookup->save();
                }
                Yii::app()->user->setFlash('success','Post wurde geupdatet');
                $this->redirect(array('update','id'=>$model->id));
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

    /**
     * @param $post_id
     */
    public function actionAddContentToPost($post_id)
    {
        $criteria = new CDbCriteria();
        $criteria->compare('post_id',$post_id);
        $count = BlogPostContent::model()->count($criteria);
        $model = new BlogPostContent();
        $count == 0?$model->order =1:$model->order = $count+1;
        $model->post_id = $post_id;
        if($model->validate())
        {
            $model->save();
            $this->redirect(array('update','id'=>$post_id));
        }
    }
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        Yii::app()->clientscript->registerScriptFile($this->module->assetsUrl.'/js/popover_info.js',CClientScript::POS_HEAD);
		$model=$this->loadPostModel($id);

        if ($model->galleryBehavior->getGallery() === null) {
            $gallery = new Gallery();
            $gallery->name = true;
            $gallery->description = true;
            $gallery->versions_data = serialize($model->galleryBehavior->versions);
            $gallery->save();
            $model->gallery_id = $gallery->id;
            $model->save();
        }

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BlogPost']))
		{
			$model->attributes=$_POST['BlogPost'];


            $model->authorName = $_POST['BlogPost']['authorName'];
            $model->categoryId  = $_POST['BlogPost']['categoryId'];
			if($model->save()){
                $criteria = new CDbCriteria();
                $criteria->compare('post_id',$model->id);
                BlogPostLookupCategory::model()->deleteAll($criteria);
                foreach($model->categoryId as $key => $category){
                    $lookup = new BlogPostLookupCategory();
                    $lookup->category_id= $category;
                    $lookup->post_id = $model->id;
                    $lookup->save();
                }
                Yii::app()->user->setFlash('success','Post wurde geupdatet');
            }

		}
		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadPostModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('BlogPost');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new BlogPost('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['BlogPost']))
			$model->attributes=$_GET['BlogPost'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

    /**
     * @param $id
     */
    public function actionSeo($id)
    {
        $model = BlogSeo::model()->findByPk($id);

        if(isset($_POST['BlogSeo']))
        {
            $model->attributes = $_POST['BlogSeo'];

            if($model->validate())
            {
                $model->update();
                Yii::app()->user->setFlash('success', 'Seo daten wurden aktualisiert');
                $this->redirect(Yii::app()->request->urlReferrer);
            }
        }
    }
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
     * @param $id
     * @return CActiveRecord
     * @throws CHttpException
     */
    public function loadPostModel($id)
	{
		$model=BlogPost::model()->with(array('author','category','blogPostContents'))->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='blog-post-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
