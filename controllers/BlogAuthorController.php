<?php
class BlogAuthorController extends EController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column1';
    public $homeUrl;

    public function init(){
        $this->homeUrl = Yii::app()->createUrl('blogadmin/blogAdmin');
    }
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view','order','create','update','admin','delete','seo'),
                'allow'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel('BlogAuthor',$id);
        if(isset($_POST['BlogAuthor']))
        {
            $model->attributes=$_POST['BlogAuthor'];
            if($model->save())
            {
                Yii::app()->user->setFlash('success','Author wurde aktualisiert');
                $this->redirect($this->homeUrl);
            }
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
     * @param $id
     */
    public function actionSeo($id)
    {
        $model = BlogSeo::model()->findByPk($id);

        if(isset($_POST['BlogSeo']))
        {
            $model->attributes = $_POST['BlogSeo'];

            if($model->validate())
            {
                $model->update();
                Yii::app()->user->setFlash('success', 'Seo daten wurden aktualisiert');
                $this->redirect(Yii::app()->request->urlReferrer);
            }
        }
    }

}