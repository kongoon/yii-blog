<?php

class BlogPostContentController extends EController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view','order','create','update','admin','delete','imageUpload','fileUpload','updateSingleContent'),
                'users'=>array('*'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel('BlogPostContent',$id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new BlogPostContent;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BlogPostContent']))
		{
			$model->attributes=$_POST['BlogPostContent'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate()
	{
		if(isset($_POST['BlogPostContent']))
		{
            foreach($_POST['BlogPostContent'] as $entry)
            {
                //CVarDumper::dump($entry,100,true);
                $post_id = $entry['post_id'];
                $model = BlogPostContent::model()->findByPk($entry['id']);
                $model->attributes = $entry;
                if($model->validate())
                {
                    $model->save();
                }
            }
            $this->redirect(array('blogPost/update','id' => $post_id));
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        $model = $this->loadModel('BlogPostContent',$id);
        $post_id = $model->post_id;
        // we only allow deletion via POST request
        $model->delete();
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        Yii::app()->user->setFlash('success','Content - Block wurde gelöscht');
        $this->redirect(array('blogPost/update','id' => $post_id));
	}
    private function get_file_extension($file_name) {
        return substr(strrchr($file_name,'.'),1);
    }
    /**
     * @param $id
     */
    public function actionFileUpload($id){
        $file_name = $id.'_'.Sanitizer::getSanitizedUrlValue($_FILES['file']['name']).'.'.$this->get_file_extension($_FILES['file']['name']);

        copy($_FILES['file']['tmp_name'], realpath(Yii::app()->basePath.'/../www/uploads/blog_content/files/').'/'.$file_name);

        $array = array(
            'filelink' => Yii::app()->baseUrl.'/uploads/blog_content/files/'.$file_name,
            'filename' => $file_name
        );

        echo stripslashes(CJSON::encode($array));
    }

    /**
     *
     */
    public function actionImageUpload($id){
        $dir = realpath(Yii::app()->basePath.'/../www/uploads/blog_content/images/');
        $_FILES['file']['type'] = strtolower($_FILES['file']['type']);
        if ($_FILES['file']['type'] == 'image/png'
            || $_FILES['file']['type'] == 'image/jpg'
            || $_FILES['file']['type'] == 'image/gif'
            || $_FILES['file']['type'] == 'image/jpeg'
            || $_FILES['file']['type'] == 'image/pjpeg')
        {
            // setting file's mysterious name
            $file_name = $id.'_'.Sanitizer::getSanitizedUrlValue($_FILES['file']['name']).'.'.$this->get_file_extension($_FILES['file']['name']);
            $file = $dir.'/'.$file_name;

            // copying
            if(!copy($_FILES['file']['tmp_name'], $file)){
                echo CJSON::encode($array = array(
                    'filelink' => Yii::app()->baseUrl.'/uploads/blog_content/images/'.$file_name,
                    'error' => 'Game over'
                ));
                Yii::app()->end();
            }

            // displaying file
            echo stripslashes(CJSON::encode($array = array(
                'filelink' => Yii::app()->baseUrl.'/uploads/blog_content/images/'.$file_name
            )));

        }
    }

    /**
     * @param $id
     */
    public function actionUpdateSingleContent($id)
    {

        if(isset($_POST['BlogPostContent']))
        {

            foreach($_POST['BlogPostContent'] as $content)
            {
                $model = $this->loadModel('BlogPostContent',$id);
                $model->content =  urldecode($content['content']);
                $model->update();
            }
            echo CJSON::encode(
                array(
                    'validate' => $model->validate(),
                    'model' => $model->attributes
                )
            );
            Yii::app()->end();
        }
    }
    /**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('BlogPostContent');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new BlogPostContent('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['BlogPostContent']))
			$model->attributes=$_GET['BlogPostContent'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
    /**
     * sort the column up and down
     * @param $id
     * @param $type
     */
    public function actionOrder($id, $type)
    {
        $model = BlogPostContent::model()->findByPk($id);
        $criteria = new CDbCriteria();
        $post_id =$model->post_id;
        if ($type == 'up') {
            $sort = $model->order;

            $criteria->condition = '`order` < ' . $sort.' AND `post_id`= '.$model->post_id;
            $criteria->order = '`order` DESC';
            if ($beforeModel = BlogPostContent::model()->find($criteria)) {
                $newSort = $beforeModel->order;

                $beforeModel->order = $sort;
                $model->order = $newSort;
                if ($beforeModel->update() && $model->update()) {
                    Yii::app()->user->setFlash('success', 'Content wurde verschoben');
                    $this->redirect(array('blogPost/update','id' => $post_id));
                    Yii::app()->end();
                }
            } else {
                Yii::app()->user->setFlash('error', 'Content kann nicht weiter nach oben verschoben werden Sie ist bereits an erste Stelle');
                $this->redirect(array('blogPost/update','id' => $post_id));
                Yii::app()->end();
            }
        }


        if ($type == 'down') {
            $sort = $model->order;
            $criteria->condition = '`order` > ' . $sort.' AND `post_id`= '.$model->post_id;
            $criteria->order = '`order` ASC';

            if ($beforeModel = BlogPostContent::model()->find($criteria)) {
                $newSort = $beforeModel->order;

                $beforeModel->order = $sort;
                $model->order = $newSort;
                if ($beforeModel->update() && $model->update()) {
                    Yii::app()->user->setFlash('success', 'Content wurde verschoben');
                    $this->redirect(array('blogPost/update','id' => $post_id));
                    Yii::app()->end();
                }
            } else {
                Yii::app()->user->setFlash('success', 'Content kann nicht weiter nach unten verschoben werden Sie ist bereits an letzter Stelle');
                $this->redirect(array('blogPost/update','id' => $post_id));
                Yii::app()->end();
            }
        }
    }
//	/**
//	 * Returns the data model based on the primary key given in the GET variable.
//	 * If the data model is not found, an HTTP exception will be raised.
//	 * @param integer the ID of the model to be loaded
//	 */
//	public function loadBlogPostContentModel($id)
//	{
//		$model=BlogPostContent::model()->findByPk($id);
//		if($model===null)
//			throw new CHttpException(404,'The requested page does not exist.');
//		return $model;
//	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='blog-post-content-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
