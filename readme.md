### Yii Blog Module for Yii Framework

#### Version
alpha -> 0.1-rc

version | create | Developer
------------ | ------------- | ------------
alpha | 13.06.2013  | Pascal Brewing

	install /data/shema.sql

	'modules' => array(
			'blogadmin' => array(
                'class' => 'application.modules.blogadmin.BlogAdminModule'
            ),
    )
    'components' => array(

        'urlManager' => array(
			'urlFormat' => 'path',
			'rules' => array(
                  .....
			        //frontend rules
			     'blog/category/<slug>' => 'blog/category',
                 'blog/author/<slug>' => 'blog/author',
                 'blog/<slug>' => 'blog/post',
                 'blog/newComment/<post_id:\d+>' => 'blog/newComment',
			),
    )
